<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dispositivos
 *
 * @ORM\Table(name="dispositivos", indexes={@ORM\Index(name="disp_user", columns={"usuario"})})
 * @ORM\Entity
 */
class Dispositivos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dispositivo", type="string", length=100, nullable=true)
     */
    private $dispositivo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="os", type="string", length=100, nullable=true)
     */
    private $os;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;


}
