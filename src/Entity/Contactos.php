<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contactos
 *
 * @ORM\Table(name="contactos")
 * @ORM\Entity
 */
class Contactos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dependencia", type="string", length=250, nullable=false)
     */
    private $dependencia;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_1", type="string", length=20, nullable=false)
     */
    private $numero1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_2", type="string", length=20, nullable=true)
     */
    private $numero2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_3", type="string", length=20, nullable=true)
     */
    private $numero3;


}
