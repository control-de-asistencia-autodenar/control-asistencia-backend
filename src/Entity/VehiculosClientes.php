<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VehiculosClientes
 *
 * @ORM\Table(name="vehiculos_clientes", indexes={@ORM\Index(name="tpv_vec", columns={"tipo_vehiculo_id"}), @ORM\Index(name="usu_veh", columns={"usuario_id"})})
 * @ORM\Entity
 */
class VehiculosClientes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="placa", type="string", length=15, nullable=false)
     */
    private $placa;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=25, nullable=false)
     */
    private $color;

    /**
     * @var \TiposVehiculos
     *
     * @ORM\ManyToOne(targetEntity="TiposVehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_vehiculo_id", referencedColumnName="id")
     * })
     */
    private $tipoVehiculo;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;


}
