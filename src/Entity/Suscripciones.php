<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Suscripciones
 *
 * @ORM\Table(name="suscripciones", indexes={@ORM\Index(name="paq_sus", columns={"paqute_adquirido_id"})})
 * @ORM\Entity
 */
class Suscripciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=false)
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_finalizacion", type="datetime", nullable=false)
     */
    private $fechaFinalizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_cliente", type="string", length=300, nullable=false)
     */
    private $nombreCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos_cliente", type="string", length=300, nullable=false)
     */
    private $apellidosCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_cliente", type="string", length=300, nullable=false)
     */
    private $correoCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="id_suscripcion", type="string", length=100, nullable=false)
     */
    private $idSuscripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_cliente", type="string", length=100, nullable=false)
     */
    private $telefonoCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="celular_cliente", type="string", length=100, nullable=false)
     */
    private $celularCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="id_cliente", type="string", length=100, nullable=false)
     */
    private $idCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="id_plan", type="string", length=100, nullable=false)
     */
    private $idPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_plan", type="string", length=100, nullable=false)
     */
    private $nombrePlan;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_plan", type="string", length=100, nullable=false)
     */
    private $descripcionPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_plan", type="string", length=300, nullable=false)
     */
    private $valorPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="tipodocumento_cliente", type="string", length=300, nullable=false)
     */
    private $tipodocumentoCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="documento_cliente", type="string", length=300, nullable=false)
     */
    private $documentoCliente;

    /**
     * @var \PaquetesAdquiridos
     *
     * @ORM\ManyToOne(targetEntity="PaquetesAdquiridos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paqute_adquirido_id", referencedColumnName="id")
     * })
     */
    private $paquteAdquirido;


}
