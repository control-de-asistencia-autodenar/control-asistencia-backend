<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntradasSalidas
 *
 * @ORM\Table(name="entradas_salidas", indexes={@ORM\Index(name="IDX_FF822A3AFCF8192D", columns={"id_usuario"})})
 * @ORM\Entity
 */
class EntradasSalidas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_entrada", type="string", length=50, nullable=true)
     */
    private $tipoEntrada;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha", type="string", length=50, nullable=true)
     */
    private $fecha;

    /**
     * @var int|null
     *
     * @ORM\Column(name="latitud", type="integer", nullable=true)
     */
    private $latitud;

    /**
     * @var int|null
     *
     * @ORM\Column(name="longitud", type="integer", nullable=true)
     */
    private $longitud;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;


}
