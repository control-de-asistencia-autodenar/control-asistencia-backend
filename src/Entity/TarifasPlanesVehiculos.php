<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TarifasPlanesVehiculos
 *
 * @ORM\Table(name="tarifas_planes_vehiculos", indexes={@ORM\Index(name="pla_tpv", columns={"plan_id"}), @ORM\Index(name="tiv_tpv", columns={"vehiculo_id"})})
 * @ORM\Entity
 */
class TarifasPlanesVehiculos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="valor", type="integer", nullable=false)
     */
    private $valor;

    /**
     * @var \Planes
     *
     * @ORM\ManyToOne(targetEntity="Planes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var \TiposVehiculos
     *
     * @ORM\ManyToOne(targetEntity="TiposVehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id")
     * })
     */
    private $vehiculo;


}
