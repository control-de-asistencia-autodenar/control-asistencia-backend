<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Excepcion
 *
 * @ORM\Table(name="excepcion")
 * @ORM\Entity
 */
class Excepcion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuario", type="integer", nullable=false)
     */
    private $idusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreusuario", type="string", length=300, nullable=false)
     */
    private $nombreusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="modulo", type="string", length=300, nullable=false)
     */
    private $modulo;

    /**
     * @var string
     *
     * @ORM\Column(name="metodo", type="string", length=300, nullable=false)
     */
    private $metodo;

    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="string", length=500, nullable=false)
     */
    private $mensaje;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoexcepcion", type="string", length=500, nullable=false)
     */
    private $tipoexcepcion;

    /**
     * @var string
     *
     * @ORM\Column(name="pila", type="string", length=900, nullable=false)
     */
    private $pila;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=200, nullable=false)
     */
    private $origen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creacionexecpcion", type="datetime", nullable=false)
     */
    private $creacionexecpcion;


}
