<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Auditoria
 *
 * @ORM\Table(name="auditoria")
 * @ORM\Entity
 */
class Auditoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idusuario", type="integer", nullable=false)
     */
    private $idusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreusuario", type="string", length=200, nullable=false)
     */
    private $nombreusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="nitusuario", type="string", length=200, nullable=false)
     */
    private $nitusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="tabla", type="string", length=200, nullable=false)
     */
    private $tabla;

    /**
     * @var string
     *
     * @ORM\Column(name="valoresrelevantes", type="string", length=800, nullable=false)
     */
    private $valoresrelevantes;

    /**
     * @var string
     *
     * @ORM\Column(name="accion", type="string", length=800, nullable=false)
     */
    private $accion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creacionauditoria", type="datetime", nullable=false)
     */
    private $creacionauditoria;

    /**
     * @var int
     *
     * @ORM\Column(name="idelemento", type="integer", nullable=false)
     */
    private $idelemento;

    /**
     * @var string
     *
     * @ORM\Column(name="origenauditoria", type="string", length=200, nullable=false)
     */
    private $origenauditoria;


}
