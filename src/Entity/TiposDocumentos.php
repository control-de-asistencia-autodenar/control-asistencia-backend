<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TiposDocumentos
 *
 * @ORM\Table(name="tipos_documentos")
 * @ORM\Entity
 */
class TiposDocumentos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=5, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=30, nullable=false)
     */
    private $nombre;


}
