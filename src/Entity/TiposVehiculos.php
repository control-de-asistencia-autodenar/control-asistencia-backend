<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TiposVehiculos
 *
 * @ORM\Table(name="tipos_vehiculos")
 * @ORM\Entity
 */
class TiposVehiculos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_vehiculo", type="string", length=120, nullable=false)
     */
    private $tipoVehiculo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img", type="string", length=300, nullable=true)
     */
    private $img;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=300, nullable=true)
     */
    private $descripcion;


}
