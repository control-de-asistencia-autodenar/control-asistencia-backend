<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hora
 *
 * @ORM\Table(name="hora", indexes={@ORM\Index(name="est_hora", columns={"estado"})})
 * @ORM\Entity
 */
class Hora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hora", type="string", length=100, nullable=false)
     */
    private $hora;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    private $estado;


}
