<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiciosConsumidos
 *
 * @ORM\Table(name="servicios_consumidos", indexes={@ORM\Index(name="anfitriona_usuario", columns={"anfitriona_id"}), @ORM\Index(name="estado_id", columns={"estado_id"}), @ORM\Index(name="paq_ser", columns={"paquete_adquirido_id"}), @ORM\Index(name="servicio_id", columns={"servicio_id"})})
 * @ORM\Entity
 */
class ServiciosConsumidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_consumo", type="datetime", nullable=false)
     */
    private $fechaConsumo;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=150, nullable=false)
     */
    private $observacion;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \PaquetesAdquiridos
     *
     * @ORM\ManyToOne(targetEntity="PaquetesAdquiridos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paquete_adquirido_id", referencedColumnName="id")
     * })
     */
    private $paqueteAdquirido;

    /**
     * @var \Servicios
     *
     * @ORM\ManyToOne(targetEntity="Servicios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="servicio_id", referencedColumnName="id")
     * })
     */
    private $servicio;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="anfitriona_id", referencedColumnName="id")
     * })
     */
    private $anfitriona;


}
