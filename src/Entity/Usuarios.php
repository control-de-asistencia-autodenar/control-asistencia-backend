<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", indexes={@ORM\Index(name="est_usu", columns={"estado_id"}), @ORM\Index(name="tip_usu", columns={"tipo_documento_id"})})
 * @ORM\Entity
 */
class Usuarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=250, nullable=false)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=250, nullable=false)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="documento", type="string", length=30, nullable=false)
     */
    private $documento;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=300, nullable=false)
     */
    private $correo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo_alterno", type="string", length=300, nullable=true)
     */
    private $correoAlterno;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", length=20, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=20, nullable=false)
     */
    private $celular;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=0, nullable=false)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha_nacimiento", type="string", length=50, nullable=false)
     */
    private $fechaNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad_residencia", type="string", length=25, nullable=false)
     */
    private $ciudadResidencia;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=150, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="contrasena", type="string", length=250, nullable=false)
     */
    private $contrasena;

    /**
     * @var string
     *
     * @ORM\Column(name="primer_ingreso", type="string", length=50, nullable=false, options={"default"="N''"})
     */
    private $primerIngreso = 'N\'\'';

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \TiposDocumentos
     *
     * @ORM\ManyToOne(targetEntity="TiposDocumentos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_documento_id", referencedColumnName="id")
     * })
     */
    private $tipoDocumento;


}
