<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaquetesAdquiridos
 *
 * @ORM\Table(name="paquetes_adquiridos", indexes={@ORM\Index(name="paq_anf", columns={"anfitriona_id"}), @ORM\Index(name="paq_est", columns={"estado_id"}), @ORM\Index(name="plan_paq", columns={"plan_id"}), @ORM\Index(name="usu_paq", columns={"usuario_id"}), @ORM\Index(name="veh_paq", columns={"vehiculo_id"})})
 * @ORM\Entity
 */
class PaquetesAdquiridos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var int|null
     *
     * @ORM\Column(name="valor_mensual", type="integer", nullable=true)
     */
    private $valorMensual;

    /**
     * @var int|null
     *
     * @ORM\Column(name="valor_anual", type="integer", nullable=true)
     */
    private $valorAnual;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \Planes
     *
     * @ORM\ManyToOne(targetEntity="Planes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="anfitriona_id", referencedColumnName="id")
     * })
     */
    private $anfitriona;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \VehiculosClientes
     *
     * @ORM\ManyToOne(targetEntity="VehiculosClientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id")
     * })
     */
    private $vehiculo;


}
