<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Servicios
 *
 * @ORM\Table(name="servicios", indexes={@ORM\Index(name="est_ser", columns={"estado_id"}), @ORM\Index(name="sub_ser", columns={"subplan_id"}), @ORM\Index(name="tipo_vh", columns={"tipo_vehiculo_id"})})
 * @ORM\Entity
 */
class Servicios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="valor", type="integer", nullable=false)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=300, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img", type="string", length=300, nullable=true)
     */
    private $img;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tiempo", type="integer", nullable=true)
     */
    private $tiempo;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \Subplanes
     *
     * @ORM\ManyToOne(targetEntity="Subplanes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subplan_id", referencedColumnName="id")
     * })
     */
    private $subplan;

    /**
     * @var \TiposVehiculos
     *
     * @ORM\ManyToOne(targetEntity="TiposVehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_vehiculo_id", referencedColumnName="id")
     * })
     */
    private $tipoVehiculo;


}
