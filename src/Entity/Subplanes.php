<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subplanes
 *
 * @ORM\Table(name="subplanes", indexes={@ORM\Index(name="est_sub", columns={"estado_id"}), @ORM\Index(name="pla_sub", columns={"plan_id"})})
 * @ORM\Entity
 */
class Subplanes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=30, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=300, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img", type="string", length=300, nullable=true)
     */
    private $img;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_servicios", type="integer", nullable=true)
     */
    private $numServicios;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \Planes
     *
     * @ORM\ManyToOne(targetEntity="Planes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;


}
