<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermisosRoles
 *
 * @ORM\Table(name="permisos_roles", indexes={@ORM\Index(name="per_pro", columns={"permiso_id"}), @ORM\Index(name="rol_pro", columns={"rol_id"})})
 * @ORM\Entity
 */
class PermisosRoles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Permisos
     *
     * @ORM\ManyToOne(targetEntity="Permisos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="permiso_id", referencedColumnName="id")
     * })
     */
    private $permiso;

    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * })
     */
    private $rol;


}
