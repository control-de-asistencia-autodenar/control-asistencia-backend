<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bahia
 *
 * @ORM\Table(name="bahia", indexes={@ORM\Index(name="FK_bahia_estados", columns={"estado"})})
 * @ORM\Entity
 */
class Bahia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_bahia", type="string", length=100, nullable=false)
     */
    private $numeroBahia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="puesto_trabajo", type="string", length=100, nullable=true)
     */
    private $puestoTrabajo;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    private $estado;


}
