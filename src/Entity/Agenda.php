<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agenda
 *
 * @ORM\Table(name="agenda", indexes={@ORM\Index(name="fk_agendador", columns={"asignada_por"}), @ORM\Index(name="fk_bahia", columns={"id_bahia"}), @ORM\Index(name="fk_estado", columns={"estado"}), @ORM\Index(name="fk_hfinal", columns={"hora_final"}), @ORM\Index(name="fk_hinicio", columns={"hora_inicio"}), @ORM\Index(name="fk_servicio", columns={"id_servicio"}), @ORM\Index(name="fk_suscripcion", columns={"id_suscripcion"}), @ORM\Index(name="fk_usuario", columns={"id_tecnico"})})
 * @ORM\Entity
 */
class Agenda
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_agendamiento", type="date", nullable=false)
     */
    private $fechaAgendamiento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_cita", type="date", nullable=false)
     */
    private $fechaCita;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo", type="string", length=100, nullable=false)
     */
    private $tiempo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacion", type="string", length=100, nullable=true, options={"default"="N''"})
     */
    private $observacion = 'N\'\'';

    /**
     * @var \Bahia
     *
     * @ORM\ManyToOne(targetEntity="Bahia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_bahia", referencedColumnName="id")
     * })
     */
    private $idBahia;

    /**
     * @var \Estados
     *
     * @ORM\ManyToOne(targetEntity="Estados")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \Hora
     *
     * @ORM\ManyToOne(targetEntity="Hora")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hora_final", referencedColumnName="id")
     * })
     */
    private $horaFinal;

    /**
     * @var \Hora
     *
     * @ORM\ManyToOne(targetEntity="Hora")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hora_inicio", referencedColumnName="id")
     * })
     */
    private $horaInicio;

    /**
     * @var \Servicios
     *
     * @ORM\ManyToOne(targetEntity="Servicios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_servicio", referencedColumnName="id")
     * })
     */
    private $idServicio;

    /**
     * @var \Suscripciones
     *
     * @ORM\ManyToOne(targetEntity="Suscripciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_suscripcion", referencedColumnName="id")
     * })
     */
    private $idSuscripcion;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="asignada_por", referencedColumnName="id")
     * })
     */
    private $asignadaPor;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tecnico", referencedColumnName="id")
     * })
     */
    private $idTecnico;


}
