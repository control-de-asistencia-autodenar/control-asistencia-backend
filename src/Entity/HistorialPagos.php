<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistorialPagos
 *
 * @ORM\Table(name="historial_pagos", indexes={@ORM\Index(name="sus_his", columns={"suscripcion_id"}), @ORM\Index(name="tar_his", columns={"tarifa_plan_id"})})
 * @ORM\Entity
 */
class HistorialPagos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_pago", type="datetime", nullable=false)
     */
    private $fechaPago;

    /**
     * @var int
     *
     * @ORM\Column(name="valor_pago", type="integer", nullable=false)
     */
    private $valorPago;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_confirmacion_payu", type="string", length=300, nullable=false)
     */
    private $codigoConfirmacionPayu;

    /**
     * @var \Suscripciones
     *
     * @ORM\ManyToOne(targetEntity="Suscripciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="suscripcion_id", referencedColumnName="id")
     * })
     */
    private $suscripcion;

    /**
     * @var \TarifasPlanesVehiculos
     *
     * @ORM\ManyToOne(targetEntity="TarifasPlanesVehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarifa_plan_id", referencedColumnName="id")
     * })
     */
    private $tarifaPlan;


}
